class Sample(object):
    name = ''
    dataset = ''
    files = []
    xsect = 0.

    def __init__(self, name='', short='', dataset='', xsect=0., files=[]):
        self.name = name
        self.short = short
        self.dataset = dataset
        self.xsect = xsect
        self.files = files

    @staticmethod
    def fromDAS(dasrequest, name='', xsect=1.):
        smp = Sample()
        smp.name = name
        for tok in dasrequest.split():
            if 'dataset=' in tok:
                smp.dataset = tok.split('=')[1]
        return smp

'''excl_susy_samples = [
  Sample.fromDAS('/GGToEE_Pt-35_Elastic_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM', name='#gamma#gamma #to e^{+}e^{-} (el.)', 0.04581),
  Sample.fromDAS('/GGToEE_Pt-35_Inel-El_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM', name='#gamma#gamma #to e^{+}e^{-} (SD)', 0.06375),
]'''
excl_susy_samples_ele = [
    Sample(
        name = '#gamma#gamma #to e^{+}e^{-} (el.)',
        short = 'ggtoee_el',
        dataset = '/GGToEE_Pt-35_Elastic_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM',
        xsect = 0.04581,
        files = [
            '/store/mc/RunIISummer20UL18NanoAODv9/GGToEE_Pt-35_Elastic_13TeV-lpair/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/130000/4A1B4DFC-95A9-EB4C-85E5-941317696685.root',
        ]),
    Sample(
        name = '#gamma#gamma #to e^{+}e^{-} (SD)',
        short = 'ggtoee_sd',
        dataset = '/GGToEE_Pt-35_Inel-El_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM',
        xsect = 0.06375,
        files = [
            '/store/mc/RunIISummer20UL18NanoAODv9/GGToEE_Pt-35_Inel-El_13TeV-lpair/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/270000/EDEA872B-2D79-8D40-BF14-8EAC9F22E2AD.root',
        ]),
    Sample(
        name = '#gamma#gamma #to e^{+}e^{-} (DD)',
        short = 'ggtoee_dd',
        dataset = '/GGToEE_Pt-35_Inel-Inel_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM',
        xsect = 0.133396,
        files = [
            '/store/mc/RunIISummer20UL18NanoAODv9/GGToEE_Pt-35_Inel-Inel_13TeV-lpair/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/260000/91522F72-A704-B141-8908-A09461A6447E.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/GGToEE_Pt-35_Inel-Inel_13TeV-lpair/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/260000/0EC2FCCF-3F38-8044-B150-DB95D5D52708.root',
        ]),
    Sample(
        name = 'DY #to e^{+}e^{-}',
        short = 'dytoee',
        dataset = '/DYToEE_M-50_NNPDF31_TuneCP5_13TeV-powheg-pythia8/RunIISummer19UL18NanoAODv2-106X_upgrade2018_realistic_v15_L1v1-v1/NANOAODSIM',
        xsect = 2137.0,
        files = [
            '/store/mc/RunIISummer19UL18NanoAODv2/DYToEE_M-50_NNPDF31_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_upgrade2018_realistic_v15_L1v1-v1/40000/DF06C547-6727-0B4A-A3F3-51C3DC8C1AD7.root',
            '/store/mc/RunIISummer19UL18NanoAODv2/DYToEE_M-50_NNPDF31_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_upgrade2018_realistic_v15_L1v1-v1/280000/CA523346-37FF-9B47-BD1F-E278140A1570.root',
            '/store/mc/RunIISummer19UL18NanoAODv2/DYToEE_M-50_NNPDF31_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_upgrade2018_realistic_v15_L1v1-v1/280000/59AB328B-F0E3-F544-98BB-E5E55577C649.root',
            '/store/mc/RunIISummer19UL18NanoAODv2/DYToEE_M-50_NNPDF31_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_upgrade2018_realistic_v15_L1v1-v1/280000/3FCF21DC-F295-754F-9FFF-557FF2195660.root',
            '/store/mc/RunIISummer19UL18NanoAODv2/DYToEE_M-50_NNPDF31_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_upgrade2018_realistic_v15_L1v1-v1/40000/84C4862F-224D-4D41-8124-6F9E40384B87.root',
        ]),
    Sample(
        name = '#gamma#gamma #to W^{+}W^{-} (el., 13.6 TeV)',
        dataset = '/GGToWW_Pt-25_El-El_13p6TeV-superchic/Run3Winter22NanoAOD-122X_mcRun3_2021_realistic_v9-v2/NANOAODSIM',
        xsect = 0.1165105,
        files = [
            '/store/mc/Run3Winter22NanoAOD/GGToWW_Pt-25_El-El_13p6TeV-superchic/NANOAODSIM/122X_mcRun3_2021_realistic_v9-v2/40000/9e65c4b7-fae1-4b51-8d00-045dab8c698a.root',
            '/store/mc/Run3Winter22NanoAOD/GGToWW_Pt-25_El-El_13p6TeV-superchic/NANOAODSIM/122X_mcRun3_2021_realistic_v9-v2/40000/5da71c68-ce77-409e-95f7-102c188c626a.root'
        ]),
]
excl_susy_samples_mu = [
    Sample(
        name = '#gamma#gamma #to #tilde{l}^{+}#tilde^{-} (100-90 GeV)',
        dataset = '',
        xsect = 0.001271,
        files = [
            '/store/user/lforthom/MCGeneration/GamGamSlSl_13TeV_MadGraph5/SUS-RunIISummer20UL16NanoAODv9-00072.root'
        ]),
    Sample(
        name = '#gamma#gamma #to #mu^{+}#mu^{-} (el.)',
        short = 'ggtomumu_el',
        dataset = '/GGToMuMu_Pt-25_Elastic_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM',
        xsect = 0.1125,
        files = [
            '/store/mc/RunIISummer20UL18NanoAODv9/GGToMuMu_Pt-25_Elastic_13TeV-lpair/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/260000/E01D240B-0FEE-7A45-80C3-2FC9C6789682.root'
        ]),
    Sample(
        name = '#gamma#gamma #to #mu^{+}#mu^{-} (SD)',
        short = 'ggtomumu_sd',
        dataset = '/GGToMuMu_Pt-25_Inel-El_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM',
        xsect = 0.1463,
        files = [
            '/store/mc/RunIISummer20UL18NanoAODv9/GGToMuMu_Pt-25_Inel-El_13TeV-lpair/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/260000/29AAA303-1DB8-3445-A477-42CE4B1EAC72.root'
        ]),
    Sample(
        name = '#gamma#gamma #to #mu^{+}#mu^{-} (DD)',
        short = 'ggtomumu_dd',
        dataset = '/GGToMuMu_Pt-25_Inel-Inel_13TeV-lpair/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM',
        xsect = 0.304945,
        files = [
            '/store/mc/RunIISummer20UL18NanoAODv9/GGToMuMu_Pt-25_Inel-Inel_13TeV-lpair/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/130000/5EFEA332-83ED-4740-A692-EFCA44969911.root'
        ]),
    Sample(
        name = 'DY #to #mu^{+}#mu^{-}',
        short = 'dytomumu',
        dataset = '/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18NanoAODv9-PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM',
        xsect = 1907.,  # after matching
        files = [
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/BC247E6E-44D0-B34C-9D04-F0C26E946FCF.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/70CA1184-E60B-4847-A423-51BA25CABF41.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/29F1F377-FB8C-AA48-B8CA-D063F495C09C.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/F5C51B5B-5644-5E4C-A6C0-13E59B96E4E1.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/FE25EFAB-4B99-2A43-8427-D1FFF6FC0259.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/42A6B032-BB20-A540-A283-3F5823EC0A8A.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/8294D646-4E85-4A46-A607-5431F2122817.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/D8B1B741-C598-F749-8384-6167DD491249.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/A8DE0023-8AF4-5649-911E-8AE55475E4F7.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/713E9F2D-693D-1F4C-AF99-D7113105649F.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/6496E2AE-EFC0-BD4D-829A-8F5F045A7AAE.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/267F7037-CDB8-264C-8C10-9F2BE14D7CAD.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/20A2B26A-7A4A-D84C-8AB3-E82D8003F3F3.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/0C6E8ACD-3FE2-034D-BBC8-3D69857635CD.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/61A5B547-D73A-8441-81CC-DB1473619B60.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/F356AA08-090E-A24E-A1D1-BD8594B9BA32.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/45F98E37-6BC6-0747-9945-ED765F572586.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/89E98D92-02DD-974F-877B-62EBEBC9CFEB.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/57DBE4A5-08A8-074B-B683-A9349CBBA467.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/C8F6D949-14E3-2545-91F0-34A74F45A16B.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/1ACD86CB-5ED5-5941-8476-FFB0E3774FC6.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/D7B12265-10DE-9D44-9EE9-5CBC72E7342D.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/0A89060C-D152-D043-94B2-1856105D2BD9.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/0E9A7546-0C21-DF4D-BDF7-5C551D4AE94C.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/D313503A-09D6-C547-B8B2-F0DF9E9659C4.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/3FF56A35-7F0C-894C-9460-4092697338D5.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/29D3836C-0693-5744-854F-D544067534B8.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/E0CFE74A-D1B9-F247-A6BF-35105E3C6205.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/0190392E-4E57-9446-9E2C-E8CAD87A35BA.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/100000/5D42AC21-A023-1147-B56F-56593186E7BC.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/2510000/BFB804D4-1C83-5F45-8B69-D9F7D3C3C08C.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/40000/730A8013-B8A9-FD4E-B2D7-76CFDA66E3C4.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/40000/DF9678F6-DDC3-114C-A015-6E6916451899.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/40000/7F774B14-4994-F144-997B-AEB1ADF199A9.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/40000/B6554B7B-2CFB-A049-835B-00B21137B72E.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/40000/CAC1D0C5-9B78-CC41-854B-3D2CBF0CBF45.root',
            '/store/mc/RunIISummer20UL18NanoAODv9/DYJetsToMuMu_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/NANOAODSIM/PUForMUOVal_106X_upgrade2018_realistic_v16_L1v1-v1/2510000/82B8D757-03DE-C140-A26F-C212F97714CF.root',
        ]),
    #Sample(
    #    name = '#gamma#gamma #to W^{+}W^{-} (el.)',
    #    dataset = '',
    #    xsect = 0.,
    #    files = [
    #        ''
    #    ]),
    Sample(
        name = '#gamma#gamma #to W^{+}W^{-} (el., 13.6 TeV)',
        dataset = '/GGToWW_Pt-25_El-El_13p6TeV-superchic/Run3Winter22NanoAOD-122X_mcRun3_2021_realistic_v9-v2/NANOAODSIM',
        xsect = 0.1165105,
        files = [
            '/store/mc/Run3Winter22NanoAOD/GGToWW_Pt-25_El-El_13p6TeV-superchic/NANOAODSIM/122X_mcRun3_2021_realistic_v9-v2/40000/9e65c4b7-fae1-4b51-8d00-045dab8c698a.root',
            '/store/mc/Run3Winter22NanoAOD/GGToWW_Pt-25_El-El_13p6TeV-superchic/NANOAODSIM/122X_mcRun3_2021_realistic_v9-v2/40000/5da71c68-ce77-409e-95f7-102c188c626a.root'
        ]),
    Sample(
        name = '#gamma#gamma #to #tilde{l}^{+}#tilde^{-} (100-90 GeV)',
        dataset = '',
        xsect = 0.001271,
        files = [
            'file:/eos/home-l/lforthom/studies/exclusive_susy/CMSSW_10_6_30/src/SusyAnalysis/SUS-RunIISummer20UL16NanoAODv9-00072.root'
        ]),
]

if __name__ == '__main__':
    print('Electron samples:\n{}'.format(excl_susy_samples_ele))
    print('Muon samples:\n{}'.format(excl_susy_samples_mu))
