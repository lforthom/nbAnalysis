# Notebook analyses

You will find here a few helpers for your nanoAOD-level analysis.

These are implemented as visual (Jupyter-like) notebooks running on the SWAN environment.
Click [![SWAN](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/lforthom/nbAnalysis.git) to download and run the notebook directly inside your environment.
