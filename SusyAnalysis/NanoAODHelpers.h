#ifndef SusyAnalysis_NanoAODHelpers_h
#define SusyAnalysis_NanoAODHelpers_h

#include "ROOT/RVec.hxx"

namespace rv = ROOT::VecOps;
namespace rm = ROOT::Math;

rv::RVec<rm::PtEtaPhiMVector> protonsFromEventContent(const rv::RVec<int>& pdgids,
                                                      const rv::RVec<int>& statuses,
                                                      const rv::RVec<float>& pts,
                                                      const rv::RVec<float>& etas,
                                                      const rv::RVec<float>& phis,
                                                      const rv::RVec<float>& masses,
                                                      float cm_energy = 13.e3,
                                                      int parton_id = 22) {
  rv::RVec<rm::PtEtaPhiMVector> protons;
  for (size_t i = 0; i < pdgids.size(); ++i) {
    if (pdgids.at(i) == 2212 && statuses.at(i) == 1)  // found one final state, stable proton
      protons.emplace_back(pts.at(i), etas.at(i), phis.at(i), masses.at(i));
    else if (pdgids.at(i) == parton_id && statuses.at(i) == 21) {
      // found an initial parton with proper the status code (21 = incoming parton for Pythia 8)
      rm::PtEtaPhiMVector parton(pts.at(i), etas.at(i), phis.at(i), masses.at(i));
      rm::PxPyPzMVector beam(0., 0., TMath::Sign(cm_energy * 0.5, etas.at(i)), 0.938);
      protons.emplace_back(beam - parton);
    }
  }
  return protons;
}

/// Quick and dirty definition of a collection of getters for a vector of ROOT's
/// LorentzVector class attributes
#define def_lv_meth(meth)                                                           \
  rv::RVec<float> lorentzVector_##meth(const rv::RVec<rm::PtEtaPhiMVector>& coll) { \
    rv::RVec<float> out;                                                            \
    for (const auto& elem : coll)                                                   \
      out.emplace_back(elem.meth());                                                \
    return out;                                                                     \
  }

def_lv_meth(px);
def_lv_meth(py);
def_lv_meth(pz);
def_lv_meth(pt);
def_lv_meth(eta);
def_lv_meth(phi);
def_lv_meth(energy);
def_lv_meth(mass);

#undef def_lv_meth

rv::RVec<float> particleMass(const rv::RVec<int>& pdgids) {
  rv::RVec<float> out;
  for (const auto& pdgid : pdgids)
    switch (abs(pdgid)) {
      case 12:
      case 14:
      case 16:
      case 21:
      case 22:
      default:
        out.emplace_back(0.);
        break;
      case 11:
        out.emplace_back(0.511e-3);
        break;
      case 13:
        out.emplace_back(0.10566);
        break;
      case 15:
        out.emplace_back(1.77686);
        break;
    }
  return out;
}

rv::RVec<bool> stableParticle(const rv::RVec<int>& pdgids, const rv::RVec<int>& statuses) {
  rv::RVec<bool> out;
  for (size_t i = 0; i < pdgids.size(); ++i)
    out.emplace_back(statuses.at(i) == 1);
  return out;
}

rv::RVec<bool> chargedLepton(const rv::RVec<int>& pdgids, const rv::RVec<int>& statuses) {
  rv::RVec<bool> out;
  for (size_t i = 0; i < pdgids.size(); ++i) {
    bool to_keep = true;
    const auto pdgid = pdgids.at(i), status = statuses.at(i);
    to_keep &= (abs(pdgid) == 11 || abs(pdgid) == 13 || abs(pdgid) == 15);
    to_keep &= (status == 1);
    out.emplace_back(to_keep);
  }
  return out;
}

rv::RVec<bool> chargedSlepton(const rv::RVec<int>& pdgids, const rv::RVec<int>& statuses) {
  rv::RVec<bool> out;
  for (size_t i = 0; i < pdgids.size(); ++i) {
    bool to_keep = true;
    const auto pdgid = pdgids.at(i), status = statuses.at(i);
    to_keep &= (abs(pdgid) / 1000000 != 0);
    const auto rem_pdgid = abs(pdgid) % 1000000;
    to_keep &= (rem_pdgid == 11 || rem_pdgid == 13 || rem_pdgid == 15);
    to_keep &= (status > 60 && status < 70);
    //to_keep &= (status == 22);
    out.emplace_back(to_keep);
  }
  return out;
}

rv::RVec<bool> neutralino(const rv::RVec<int>& pdgids, const rv::RVec<int>& statuses) {
  rv::RVec<bool> out;
  for (size_t i = 0; i < pdgids.size(); ++i) {
    bool to_keep = true;
    const auto pdgid = pdgids.at(i), status = statuses.at(i);
    to_keep &= (abs(pdgid) == 1000022 || abs(pdgid) == 1000023 || abs(pdgid) == 1000024);
    to_keep &= (status == 1);
    out.emplace_back(to_keep);
  }
  return out;
}

rv::RVec<float> xiFromMomenta(const rv::RVec<float>& pts,
                              const rv::RVec<float>& etas,
                              const rv::RVec<float>& phis,
                              const rv::RVec<float>& masses,
                              const rv::RVec<int>& proton_sides,
                              float sqrt_s = 13.e3) {
  rv::RVec<float> xis(proton_sides.size(), 0.);
  size_t j = 0;
  for (const auto& proton_side : proton_sides) {
    const auto norm_proton_side = proton_side / abs(proton_side);
    for (size_t i = 0; i < pts.size(); ++i) {
      const auto mom = rm::PtEtaPhiMVector(pts.at(i), etas.at(i), phis.at(i), masses.at(i));
      if (mom.Mt() >= 0. && !isinf(mom.Rapidity()))
        xis[j] += mom.Mt() / sqrt_s * exp(norm_proton_side * mom.Rapidity());
    }
    ++j;
  }
  return xis;
}

rv::RVec<float> xiFromMomenta(const rv::RVec<float>& pzs, double cm_energy = 13.e3) {
  rv::RVec<float> xis;
  for (const auto& pz : pzs)
    xis.emplace_back(1. - 2. * fabs(pz) / cm_energy);
  return xis;
}

rv::RVec<int> signFromEta(const rv::RVec<float>& etas) {
  rv::RVec<int> signs;
  for (const auto& eta : etas)
    signs.emplace_back(TMath::Sign(1, eta));
  return signs;
}

rv::RVec<float> ptFromPxPy(const rv::RVec<float>& pxs, const rv::RVec<float>& pys) {
  assert(pxs.size() == pys.size());
  rv::RVec<float> pts;
  for (size_t i = 0; i < pxs.size(); ++i)
    pts.emplace_back(std::hypot(pxs.at(i), pys.at(i)));
  return pts;
}

rv::RVec<float> etaFromPtPz(const rv::RVec<float>& pts, const rv::RVec<float>& pzs) {
  assert(pts.size() == pzs.size());
  rv::RVec<float> etas;
  for (size_t i = 0; i < pts.size(); ++i)
    etas.emplace_back(pts.at(i) != 0. ? std::asinh(pzs.at(i) / pts.at(i)) : -999.999);
  return etas;
}

rv::RVec<float> phiFromPxPy(const rv::RVec<float>& pxs, const rv::RVec<float>& pys) {
  assert(pxs.size() == pys.size());
  rv::RVec<float> phis;
  for (size_t i = 0; i < pxs.size(); ++i)
    phis.emplace_back(std::atan2(pys.at(i), pxs.at(i)));
  return phis;
}

#endif
